(defmacro measure-time (&rest body)
  "Measure and return the running time of the code block."
  (declare (indent defun))
  (let ((start (make-symbol "start")))
    `(let ((,start (float-time)))
       ,@body
       (- (float-time) ,start))))

(require 'liberime)
(rime-start "/usr/share/rime-data" (expand-file-name "~/.emacs.d/rime"))
(rime-search "wode")
